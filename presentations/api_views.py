from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Presentation
from common.json import ModelEncoder
import json


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]

    def get_extra_data(self, o):
        return {"status": o.status.name}


@require_http_methods(["DELETE", "GET", "PUT"])
def api_list_presentations(request, conference_id):
    presentation = Presentation.objects.get(id=pk)
    pass


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, pk):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=pk)
        return JsonResponse(presentation, encoder=PresentationDetailEncoder)
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=pk).delete()
        return JsonResponse(
            presentation, encoder=PresentationDetailEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        Presentation.objects.filter(id=pk).update(**content)
        presentation = Presentation.objects.get(id=pk)
        return JsonResponse(
            presentation, encoder=PresentationDetailEncoder, safe=False
        )
