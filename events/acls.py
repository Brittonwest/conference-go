import json
import requests

from .keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY


def get_weather_data(city, state):
    params = {
        "q": f"{city}, {state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = ""
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = ""
    response = requests.url(url, params=params)
    return {
        "description": content["weather"][0]["description"],
        "temp": content["main"]["temp"],
    }


# picture_url = content["photos"][0]["src"]["original"]


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = ""
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
